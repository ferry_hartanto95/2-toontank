// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TankGameMoceBase.generated.h"

/**
 * 
 */
class APawnTurret;
class APawnTank;
class APlayerControllerBase;

UCLASS()
class TOONTANKS_API ATankGameMoceBase : public AGameModeBase
{
	GENERATED_BODY()
private:
	APawnTank* playerTank;
	int32 targetTurrets = 0;
	APlayerControllerBase* playerControllerRef;
	int32 GetTargetTurretCount();

	void HandleGameStart();
	void HandleGameOver(bool playerWon);
	
public:

	void ActorDied(AActor* deadActor);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Loop")
	int32 startDelay = 3;

	virtual void BeginPlay() override;
	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();
	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool playerWon);
};
