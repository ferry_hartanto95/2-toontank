// Fill out your copyright notice in the Description page of Project Settings.


#include "TankGameMoceBase.h"
#include "ToonTanks/Pawns/PawnTank.h"
#include "ToonTanks/Pawns/PawnTurret.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/PlayerController/PlayerControllerBase.h"

int32 ATankGameMoceBase::GetTargetTurretCount()
{
	TArray<AActor*> TurretActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), TurretActors);
	return TurretActors.Num();

}

void ATankGameMoceBase::HandleGameStart()
{
	//Get turret and player paen detail
	targetTurrets = GetTargetTurretCount();
	playerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
	playerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
	
	GameStart();
	if (playerControllerRef)
	{
		playerControllerRef->SetPlayerEnabledState(false);
		FTimerHandle playerEnableHandle;
		FTimerDelegate playerEnableDelegate = FTimerDelegate::CreateUObject(playerControllerRef, &APlayerControllerBase::SetPlayerEnabledState, true);
		GetWorld()->GetTimerManager().SetTimer(playerEnableHandle, playerEnableDelegate, startDelay, false);
	}
}

void ATankGameMoceBase::HandleGameOver(bool playerWon)
{
	GameOver(playerWon);
}

void ATankGameMoceBase::ActorDied(AActor* deadActor)
{
	if (deadActor == playerTank)
	{
		playerTank->HandleDestruction();
		HandleGameOver(false);

		if (playerControllerRef)
		{
			playerControllerRef->SetPlayerEnabledState(false);
		}
	}
	else if (APawnTurret* destroyedTurret = Cast<APawnTurret>(deadActor))
	{
		destroyedTurret->HandleDestruction();

		if (--targetTurrets == 0)
		{
			HandleGameOver(true);
		}
	}
}

void ATankGameMoceBase::BeginPlay()
{
	Super::BeginPlay();
	HandleGameStart();
}
