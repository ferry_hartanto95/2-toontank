	// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBase.h"
#include "Components/CapsuleComponent.h"
#include "ToonTanks/Actors/ProjectileBase.h"
#include "ToonTanks/Components/HealthComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APawnBase::APawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create the base to set the mesh in scene

	capsuleCompo = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
	RootComponent = capsuleCompo;

	baseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	baseMesh->SetupAttachment(RootComponent);

	turretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
	turretMesh->SetupAttachment(baseMesh);

	projectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	projectileSpawnPoint->SetupAttachment(turretMesh);

	healthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Components"));
}

void APawnBase::RotateTurret(FVector LookAtTarget)
{
	FVector lookAtTargetCleaned = FVector(LookAtTarget.X, LookAtTarget.Y, turretMesh->GetComponentLocation().Z);
	FVector startLocation = turretMesh->GetComponentLocation();

	FRotator turretRotation = FVector(lookAtTargetCleaned - startLocation).Rotation();
	turretMesh->SetWorldRotation(turretRotation);
}

void APawnBase::Fire()
{
	//UE_LOG(LogTemp, Warning, TEXT("Fire Condition Checked"));
	
	if (projectileClass)
	{
		FVector spawnLocation = projectileSpawnPoint->GetComponentLocation();
		FRotator spawnRotation = projectileSpawnPoint->GetComponentRotation();
		
		AProjectileBase* tempProjectile = GetWorld()->SpawnActor<AProjectileBase>(projectileClass, spawnLocation, spawnRotation);
		tempProjectile->SetOwner(this);
		
	}
}

void APawnBase::HandleDestruction()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, deathParticle, GetActorLocation());
	UGameplayStatics::SpawnSoundAtLocation(this, deathSound, GetActorLocation());

	GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(deathShake);
}




