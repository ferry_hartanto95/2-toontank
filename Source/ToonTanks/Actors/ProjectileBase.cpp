// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

void AProjectileBase::OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	//get owner
	AActor* myOwner = GetOwner();
	if (!myOwner)
	{
		return;
	}
	//calculate damage if not owner and self
	if (otherActor && otherActor != this && otherActor != myOwner)
	{
		UGameplayStatics::ApplyDamage(otherActor,damage,myOwner->GetInstigatorController(),this,damageType);
	}
	//spawn emmiter partcileand sound
	UGameplayStatics::SpawnEmitterAtLocation(this, hitParticle, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(this, hitSound, GetActorLocation());
	//apply camera shake
	GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(hitShake);
	Destroy();

}

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//create the base to apply the mesh in scene
	projectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	
	RootComponent = projectileMesh;

	projectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	//set the speed for projectile
	projectileMovement->InitialSpeed = movementSpeed;
	projectileMovement->MaxSpeed = movementSpeed;
	InitialLifeSpan = 3.0f;

	//setparticle for projectile trail
	particleTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Trail"));
	particleTrail->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	projectileMesh->OnComponentHit.AddDynamic(this, &AProjectileBase::OnHit);
	UGameplayStatics::PlaySoundAtLocation(this, launchSound, GetActorLocation());
}


